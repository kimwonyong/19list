var app = new Vue({
  el: '#app',
  data: {
    dates: [],
    prev: {},
    today: moment(),
    startdate: moment(),
    enddate: moment().add(10, 'year')
  },
  mounted: function() {
    var that = this;

    // 최초 19금 목록
    that.getList(that.startdate, that.enddate);
    that.dates[0].active = true;
    that.prev = that.dates[0];

    // 클립보드 복사
    var clipboard = new Clipboard('#clipboard');
    clipboard.on('success', function(e) {
      Materialize.toast('주소가 복사되었습니다.', 4000);
    });
    clipboard.on('error', function(e) {
      Materialize.toast('죄송합니다. 주소창에서 복사해주세요.', 4000);
    });

    // 페이스북 SDK
    window.fbAsyncInit = function() {
      FB.init({
        appId: '2010939329142490',
        xfbml: true,
        version: 'v2.10'
      });
      FB.AppEvents.logPageView();
    };

    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {
        return;
      }
      js = d.createElement(s);
      js.id = id;
      js.src = "//connect.facebook.net/ko_KR/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


    // 카카오톡
    Kakao.init('33be955c9e41e1db04f42c95fc09d297');



    // 애드센스
    (adsbygoogle = window.adsbygoogle || []).push({});
    (adsbygoogle = window.adsbygoogle || []).push({});
    (adsbygoogle = window.adsbygoogle || []).push({
      google_ad_client: "ca-pub-4160236743425285",
      enable_page_level_ads: true
    });

  },
  methods: {
    btnKakao: function() {
      Kakao.Link.sendCustom({
        templateId: 4923
      });
    },
    btnFacebook: function() {
      FB.ui({
        method: 'share',
        display: 'popup',
        href: 'http://19.circledragon.co.kr/',
      }, function(response) {});
    },
    btnClipboard: function() {
      $('#clipboard').trigger('click');
    },
    gotoDate: function(date) {
      var that = this;
      that.prev.active = false;
      date.active = true;
      that.prev = date;
    },
    btnPrevious: function() {
      var that = this;
      that.getList(moment(that.startdate).subtract(10, 'year'), that.startdate, 'prev');
      that.startdate = moment(that.startdate).subtract(10, 'year');
    },
    btnNext: function() {
      var that = this;
      that.getList(that.enddate, moment(that.enddate).add(10, 'year'), 'next');
      that.enddate = moment(that.enddate).add(10, 'year');
    },
    getList: function(start, end, type) {
      var that = this;
      var start = moment(start);
      var end = moment(end);
      var dates = [];

      while (start < end) {
        if (start.format('e') === '5' && start.format('DD') === '19') {
          var dday = that.today.diff(start, 'day');
          if (dday === 0) {
            dday = 'NOW';
          } else if (dday > 0) {
            dday = 'D + ' + dday;
          } else {
            dday = 'D - ' + Math.abs(dday - 1);
          }
          dates.push({
            moment: start,
            formated: start.format('YYYY년 MM월 DD일 금요일'),
            start: start.format('YYYY-MM-DD'),
            dday: dday,
            active: false
          });
        }
        start.add(1, 'day');
      }

      if (type === 'prev') {
        that.dates = dates.concat(that.dates);
      } else {
        that.dates = that.dates.concat(dates);
      }
    }
  }
})
